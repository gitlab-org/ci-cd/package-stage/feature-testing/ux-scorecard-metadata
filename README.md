# ux scorecard metadata

A project to test investigating dev dependency meta data and historical information


## Set up steps taken
### Publishing a npm package via David's package generator
1. :exclamation: Some stumbling blocks hit, detailed in this [issue](https://gitlab.com/10io/gl_pru/-/issues/2)

### Following docs to publish a package with ci/cd 
1. Started on [Publish an npm package by using CI/CD](https://docs.gitlab.com/ee/user/packages/npm_registry/#publish-an-npm-package-by-using-cicd)
1. I was then redirect to [Authenticate](https://docs.gitlab.com/ee/user/packages/npm_registry/#authenticate-to-the-package-registry)<br>
:exclamation: I would not have known to create a `.npmrc` file to paste the project level endpoint. I only know this as I spent several hours going through youtube videos recently.
1. Was interested in following a step-by-step tutorial, so clicked on [publish npm packages with semantic release](https://docs.gitlab.com/ee/ci/examples/semantic-release.html)<br>
:exclamation: I don't understand the instruction: _Add the following properties to the module’s package.json_ I don't understand what the module is.<br> 
:exclamation: Expectedly the pipeline failed. I not really understanding the docss there are a number of mistakes I could have made.

### Changing course 
1. I think semantic release is too detailed for what I need, so I'm going to roll back and try a different way of publishing a package with CI/CD. I removed most everything out of the project
1. I installed a package in my project from the NPM public registry
1. I used the yml template for npm and got a package to build sucessfully
1. I added another package from the package registry and the build has failed, I suspect it has something to do with the lack of an .npmrc file 
1. Pipeline fails due to something wrong with publish URL, it's intermixing gitlab and npm URL. Will need to dig more into the docs to understand how to correct
1. :exclamation: spent half the day getting `npm 403` when trying to publish a package. Was only able to resolve with a youtube tutorial that sets up the `.npmrc` in a different way to the doucmentation.

**Documentation**<br>
``
npm config set @foo:registry https://gitlab.example.com/api/v4/projects/<your_project_id>/packages/npm/
``<br>
``
npm config set -- '//gitlab.example.com/api/v4/projects/<your_project_id>/packages/npm/:_authToken' "<your_token>"
``

**What actually worked**<br>
``
@gitlab-org:registry https://gitlab.com/api/v4/packages/npm/
``<br>
``
//gitlab.com/api/v4/packages/npm/:_authToken=TOKEN-HERE
``<br>
``
//gitlab.com/api/v4/projects/29589722/packages/npm/:_authToken=TOKEN-HERE
``

I don't understand why the above worked and it was hard to figure out. 

### Publish from pipeline
1. :exclamation: When I bump versions it creates a another package on the list of packages. I would expect it to create the other versions tab on the package detail package
1. :exclamation: I publish a package with my pipeline, but it says manually uploaded and I don't know why
1. I'm going to look more into the docs about [publishing with cicd](https://docs.gitlab.com/ee/user/packages/npm_registry/#publish-an-npm-package-by-using-cicd)
1. :exclamation: I don't understand why the ci.yml from the docs is so different than the template I got from gitlab
1. I need more hand holding so I will try again to [publish npm packages with semantic release](https://docs.gitlab.com/ee/ci/examples/semantic-release.html)
1. :exclamation: I don't know what files are expected in this bit to add to my package.json `  "files": [ <path(s) to files here> ]` + I don't know what it means by "modules package.json" so I'm assuming it just means the package.json I created with `npm init`
1.  :exclamation: Adding the properties to package.json creates invalid json, so I'm probably pasting it in the wrong place. Given that I have no file paths, I remove that part which fixes the json
1.  :exclamation: The copy/paste has a different `publishConfig`, I let this override what I previously had there with the scoped registry.
1. I override my `.gitlab-ci.yml` with the one from these docs.
1. I've already set up CI/CD variable 
1. I create a `.releaserc.json` 
1. :exclamation: docs for `.releaserc.json` mention master branch but I think all new projects are created with main now? Could be easy to mistake
1. :exclamation: pipeline fails on `npm ERR! 401 Unauthorized`
1. After checking out some issues I suspect the problem is that I overrode the `publishConfig`
1. Still broken after changing publishConfig, found a [different gitlab-ci.yml](https://gitlab.com/sabrams/npm-practice/-/blob/master/.gitlab-ci.yml#L76), will give that a try
1. :exclamation: New pipeline also fails on `npm ERR! 403 403 Forbidden`. I'm going to try to bump the version and see if the error is caused by trying to publish a duplicate package rather then an authentication issue 
1. The pipeline passes with a bumped verison but I still don't understand why it shows in the UI as manually published and also why it creates a new entry in the packages table (rather than listing in the tab "Other versions"). I also notice with the `gitlab-ci.yml` it tries to run the pipeline with each commit, which is not very handy. From the template `gitlab-ci.yml` it had some argument like "if package.json is changed run the pipeline" which was much more helpful. I'm going to compare the `yml` template versus the one I got from the example project. I switch back to the default template which works but again bumping the version creates another entry in the list of packages. 
1. :exclamation: I've spent a while reading the documentation and I don't know how to get packages versions to appear in "other versions" or why my packages are listed as manually uploaded. 
1. I now see that the versions of the package are listed **both** on the list of all packages and on the tab "other versions". I didn't expect that.
1. I'm replacing my `gitlab-ci.yml` with the one that actually shows my package being built from `main` (as opposed to manually published). Job suceeds but still listed as manually published and I don't know why. I'm also unsure how the latest tag gets set (probably upload date???) and what other tags are available.

### Install a package 
1. I'm going to try installing a package with the instructions in the [documentation](https://docs.gitlab.com/ee/user/packages/npm_registry/index.html)
1. :exclamation: The first thing I notice is the documentation has me set the URL for scoped packages **first** but the package detail page doesn't have this.
1. I'm able to install the package. Part of the documentation mentions needing to be authenticated if the project is not public. That links back to the authentication section which I'm now thinking could be run in the command line rather than in the `.npmrc` ? :woman_facepalming: The entire time I thought the authentication needed to happen in the `.npmrc` but it works in the command line too. 
1. I'm also curious what happens without authentication, e.g. if I was not following the docs but the package detail page. I create an empty directory separate from my project and `npm init` to make `package.json`. I run `npm i @gitlab-org/one-liner` and the `@gitlab-org/one-liner` is added as a dependency in the `package.json`. I wonder if I try to build this if something will go wrong.
